CREATE DATABASE webtree;
CREATE USER 'webtree'@'%' IDENTIFIED BY 'webtree';
GRANT ALL PRIVILEGES ON webtree.* TO 'webtree'@'%';
USE webtree;
CREATE TABLE node
(
    id   INTEGER NOT NULL AUTO_INCREMENT,
    data TEXT    NOT NULL,
    p_id INTEGER NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (p_id) references node (id) on delete cascade on update no action
);

INSERT INTO node(data) VALUE ('root 1');
INSERT INTO node(data) VALUE ('root 2');

INSERT INTO node(data, p_id) VALUE ('node 1.1', 1);
INSERT INTO node(data, p_id) VALUE ('node 1.2', 1);
INSERT INTO node(data, p_id) VALUE ('node 2.1', 2);
INSERT INTO node(data, p_id) VALUE ('node 2.2', 2);
INSERT INTO node(data, p_id) VALUE ('node 2.3', 2);
INSERT INTO node(data, p_id) VALUE ('node 2.4', 2);
INSERT INTO node(data, p_id) VALUE ('node 2.2.1', 6);
INSERT INTO node(data, p_id) VALUE ('node 2.2.1.1', 9);