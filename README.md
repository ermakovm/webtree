User interfaces created with JSTree plugin.

To create a root node, use the button at the top of the screen.

You can use the context menu to create child elements, change or delete the current element.

To change the parent element, use the drag & drop

Starting:
1) gradle war
2) docker-compose up -d

Open in browser 127.0.0.1:8080/webtree