FROM tomcat:9.0-jdk11-openjdk
LABEL maintainer="mikhail@mermakov.info"
ADD ./build/libs/webtree.war /usr/local/tomcat/webapps/
EXPOSE 8080
CMD ["catalina.sh", "run"]