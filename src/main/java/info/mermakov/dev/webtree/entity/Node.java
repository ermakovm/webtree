package info.mermakov.dev.webtree.entity;

import lombok.Data;

@Data
public class Node {
    private int id;
    private int parent;
    private String text;
    private boolean children;

    public Node(int id, int parent, String text) {
        this.id = id;
        this.parent = parent;
        this.text = text;
        this.children = true;
    }
}
