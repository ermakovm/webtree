package info.mermakov.dev.webtree.servlet;

import info.mermakov.dev.webtree.model.Tree;
import info.mermakov.dev.webtree.model.impl.TreeImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class NodeMoveServlet extends HttpServlet {
    private Tree treeData;

    @Override
    public void init() throws ServletException {
        super.init();
        treeData = TreeImpl.getInstance();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("nodeid"));
        String parent = req.getParameter("parent");
        if (parent.contains("#")) {
            treeData.updateNode(id, 0);
        } else {
            treeData.updateNode(id, Integer.parseInt(parent));
        }
    }
}
