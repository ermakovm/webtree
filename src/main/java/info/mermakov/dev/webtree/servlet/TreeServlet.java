package info.mermakov.dev.webtree.servlet;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import info.mermakov.dev.webtree.entity.Node;
import info.mermakov.dev.webtree.model.Tree;
import info.mermakov.dev.webtree.model.impl.TreeImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class TreeServlet extends HttpServlet {
    private Tree treeData;

    @Override
    public void init() throws ServletException {
        super.init();
        treeData = TreeImpl.getInstance();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json; charset=utf-8");
        String id = req.getParameter("nodeid");
        try (PrintWriter out = resp.getWriter()) {
            TimeUnit.SECONDS.sleep(2);
            if (id.equals("root")) {
                List<Node> data = treeData.getRoot();
                Gson gson = new Gson();
                JsonElement element = gson.toJsonTree(data);
                JsonArray array = element.getAsJsonArray();
                for (int i = 0; i < array.size(); i++) {
                    JsonElement e = array.get(i);
                    JsonObject obj = e.getAsJsonObject();
                    obj.addProperty("parent", "#");
                }
                String json = gson.toJson(element);
                out.println(json);
            } else {
                List<Node> data = treeData.getById(Integer.parseInt(id));
                String json = new Gson().toJson(data);
                out.println(json);
            }
        } catch (IOException | InterruptedException exception) {
            exception.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("nodeid"));
        String text = req.getParameter("text");
        int res = treeData.addNode(id, text);
        try (PrintWriter out = resp.getWriter()) {
            out.print(res);
        }
    }
}
