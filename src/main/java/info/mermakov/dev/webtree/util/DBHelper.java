package info.mermakov.dev.webtree.util;

import info.mermakov.dev.webtree.entity.Node;

import java.io.Closeable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DBHelper implements Closeable {
    private Connection connection;
    private Statement statement;
    private ResultSet resultSet;

    public DBHelper() throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        String connLine = System.getProperty("db.server");
        String login = System.getProperty("db.user");
        String pass = System.getProperty("db.password");
        connection = DriverManager.getConnection(connLine, login, pass);
        statement = connection.createStatement();
    }

    public List<Node> getData(int id) throws SQLException {
        List<Node> result = new ArrayList<>();
        String getDataQuery = id == 0 ? "SELECT * FROM node WHERE p_id IS NULL" : "SELECT * FROM node WHERE p_id='" + id + "'";
        resultSet = statement.executeQuery(getDataQuery);
        while (resultSet.next()) {
            int nodeId = resultSet.getInt("id");
            String text = resultSet.getString("data");
            int parent = resultSet.getInt("p_id");
            result.add(new Node(nodeId, parent, text));
        }
        return result;
    }

    public void deleteData(int id) throws SQLException {
        String deleteQuery = "DELETE FROM node WHERE id='" + id + "'";
        statement.execute(deleteQuery);
    }

    public int addData(String text, int parent) throws SQLException {
        String addQuery = parent == 0 ? "INSERT INTO node(data) VALUE('" + text + "')" : "INSERT INTO node(data,p_id) VALUE ('" + text + "','" + parent + "')";
        statement.execute(addQuery);
        resultSet = statement.executeQuery("SELECT LAST_INSERT_ID() AS 'id'");
        int nodeId = 0;
        if (resultSet.next()) {
            nodeId = resultSet.getInt("id");
        }
        return nodeId;
    }

    public void updateData(int nodeId, int newParentId) throws SQLException {
        String updateQuery = newParentId == 0 ? "UPDATE node SET p_id=NULL" : "UPDATE node SET p_id='" + newParentId + "' WHERE id='" + nodeId + "'";
        statement.execute(updateQuery);
    }

    public void updateData(int nodeId, String text) throws SQLException {
        String updateQuery = "UPDATE node SET data='" + text + "' WHERE id='" + nodeId + "'";
        statement.execute(updateQuery);
    }

    public void close() {
        try {
            if (resultSet != null) {
                resultSet.close();
            }
            statement.close();
            connection.close();
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }
}