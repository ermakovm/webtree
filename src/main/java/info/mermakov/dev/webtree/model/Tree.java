package info.mermakov.dev.webtree.model;

import info.mermakov.dev.webtree.entity.Node;

import java.util.List;

public interface Tree {
    List<Node> getRoot();

    List<Node> getById(int parentId);

    int addNode(int parentId, String text);

    void deleteNode(int nodeId);

    void updateNode(int nodeId, int parentId);

    void updateNode(int nodeId, String text);
}
