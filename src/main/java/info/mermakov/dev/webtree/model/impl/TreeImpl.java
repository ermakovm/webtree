package info.mermakov.dev.webtree.model.impl;

import info.mermakov.dev.webtree.entity.Node;
import info.mermakov.dev.webtree.model.Tree;
import info.mermakov.dev.webtree.util.DBHelper;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TreeImpl implements Tree {
    private static volatile TreeImpl instance;

    private TreeImpl() {
    }

    public static TreeImpl getInstance() {
        TreeImpl local = instance;
        if (local == null) {
            synchronized (TreeImpl.class) {
                local = instance;
                if (local == null) {
                    instance = local = new TreeImpl();
                }
            }
        }
        return local;
    }

    public List<Node> getRoot() {
        List<Node> result = new ArrayList<>();
        try (DBHelper db = new DBHelper()) {
            result = db.getData(0);
        } catch (SQLException | ClassNotFoundException exception) {
            exception.printStackTrace();
        }
        return result;
    }

    public List<Node> getById(int id) {
        List<Node> result = new ArrayList<>();
        try (DBHelper db = new DBHelper()) {
            result = db.getData(id);
        } catch (SQLException | ClassNotFoundException exception) {
            exception.printStackTrace();
        }
        return result;
    }

    public void deleteNode(int nodeId) {
        try (DBHelper db = new DBHelper()) {
            db.deleteData(nodeId);
        } catch (SQLException | ClassNotFoundException exception) {
            exception.printStackTrace();
        }
    }

    public void updateNode(int nodeId, int parentId) {
        try (DBHelper db = new DBHelper()) {
            db.updateData(nodeId, parentId);
        } catch (SQLException | ClassNotFoundException exception) {
            exception.printStackTrace();
        }
    }

    public void updateNode(int nodeId, String text) {
        try (DBHelper db = new DBHelper()) {
            db.updateData(nodeId, text);
        } catch (SQLException | ClassNotFoundException exception) {
            exception.printStackTrace();
        }
    }

    public int addNode(int parentId, String text) {
        int result = 0;
        try (DBHelper db = new DBHelper()) {
            result = db.addData(text, parentId);
        } catch (SQLException | ClassNotFoundException exception) {
            exception.printStackTrace();
        }
        return result;
    }
}