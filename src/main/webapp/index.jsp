<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>WebTree</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.3.9/themes/default/style.min.css" rel="stylesheet"/>
</head>
<body>
<script
        crossorigin="anonymous"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.3.9/jstree.min.js"></script>
<script>
    var prevNode;
    $(function () {
        $('#jstree')
            .jstree({
                'core': {
                    'data': {
                        'url': function (node) {
                            return node.id === '#' ? 'tree?nodeid=root' : 'tree?nodeid=' + node.id;
                        },
                        'data': function (node) {
                            return {'id': node.id};
                        }
                    },
                    'check_callback': true,
                    'themes': {
                        'variant': 'large'
                    }
                },
                "plugins": ["json_data", "wholerow", "contextmenu", "dnd"],
                "types": {
                    'valid_children': [],
                    'types': {
                        'default': {
                            'valid_children': 'none'
                        },
                        'root': {
                            'valid_children': ''
                        }
                    }
                },
                "contextmenu": {
                    'items': function () {
                        return {
                            'Create': {
                                'separator_before': false,
                                'separator_after': false,
                                'label': 'Create',
                                'action': function (data) {
                                    var inst = $.jstree.reference(data.reference),
                                        obj = inst.get_node(data.reference);
                                    inst.create_node(obj, {}, "last", function (new_node) {
                                        try {
                                            inst.edit(new_node);
                                        } catch (ex) {
                                            setTimeout(function () {
                                                inst.edit(new_node);
                                            }, 0);
                                        }
                                    });
                                }
                            },
                            'Rename': {
                                'separator_before': false,
                                'separator_after': false,
                                'label': "Rename",
                                'action': function (data) {
                                    var inst = $.jstree.reference(data.reference),
                                        obj = inst.get_node(data.reference);
                                    inst.edit(obj);
                                }
                            },
                            'Remove': {
                                'separator_before': false,
                                'separator_after': false,
                                'label': "Remove",
                                'action': function (data) {
                                    var inst = $.jstree.reference(data.reference),
                                        obj = inst.get_node(data.reference);
                                    if (inst.is_selected(obj)) {
                                        inst.delete_node(inst.get_selected());
                                    } else {
                                        inst.delete_node(obj);
                                    }
                                }
                            }
                        };
                    }
                }
            })
            .on('delete_node.jstree', function (e, data) {
                $.post('delete', {'nodeid': data.node.id})
                    .fail(
                        function () {
                            data.instance.refresh();
                        }
                    );
            })
            .on('rename_node.jstree', function (e, data) {
                $.post('update', {'nodeid': data.node.id, 'text': data.text})
                    .fail(
                        function () {
                            data.instance.refresh();
                        }
                    );
            })
            .on('create_node.jstree', function (e, data) {
                $.post('tree', {'nodeid': data.node.parent, 'text': data.node.text})
                    .done(
                        function (d) {
                            console.log(d);
                            console.log(data.node);
                            data.instance.set_id(data.node, d);
                        }
                    )
                    .fail(
                        function () {
                            data.instance.refresh();
                        }
                    );
            })
            .on('move_node.jstree', function (e, data) {
                $.post('move', {'nodeid': data.node.id, 'parent': data.parent})
                    .fail(
                        function () {
                            data.instance.refresh();
                        }
                    );
            })
            .on('changed.jstree', function (e, data) {
                if (prevNode != null) {
                    changeTree(prevNode);
                }
                prevNode = data.instance.get_node(data.selected[0]);
                changeTree(data.instance.get_node(data.selected[0]));
            })
        $('#frmRoot').submit(function (event) {
            var $frmRoot = $(this);
            $.ajax({
                type: $frmRoot.attr('method'),
                url: $frmRoot.attr('action'),
                data: $frmRoot.serialize()
            })
                .done(function () {
                    console.log('ok')
                    $('#jstree').jstree(true).refresh();
                })
                .fail(function () {
                    console.log("fail")
                });
            event.preventDefault();
        });
    });


    function changeTree(changeNode) {
        if ($('#jstree').jstree(true).is_selected(changeNode.id)) {
            $('#jstree').jstree(true).set_ico
            $('#jstree').jstree(true).set_icon(changeNode.id, './static/themes/default/1.png');
        } else {
            $('#jstree').jstree(true).set_icon(changeNode.id, true);
        }
    }


</script>
<div>
    <form id="frmRoot" action="./tree" method="post">
        <input type="hidden" name="nodeid" value="0">
        <label>Enter text for new root node:
            <input type="text" name="text" required>
        </label>
        <button type="submit">Create node</button>
    </form>
    <br>
</div>
<div id="jstree"></div>
</body>
</html>
